<h1><div align="center">Project 1999 Bot - Project 1999 Container</div></h1>

[TOC]

# Overview

This container builds upon the operating system container, adding
in tools that enable the execution and management of a Project
1999 EverQuest client and supporting apps. The apps managed by
this container are:

* **p99-login-middlemand**: A service that intercepts and fixes
  the login server list, which does not work properly on Linux
  machines.
* **xpra**: A service that provides a mechanism to run X11 apps,
  including an HTTP interface and GPU emulation.
* **p99**: The Project 1999 EverQuest client, executed via WINE.

For each of these apps, the container attempts to ensure that the
app is always running, restarting it if it stops. Typically this
is done by a simple shell loop.

# Creation

## Building the Container

For basic information about how to build these containers, see
the main project repo[^1].

The container is built from a Dockerfile which is based on the
p99bot-os container image. This Dockerfile additionally installs
xpra, WINE, p99-login-middlemand, the Project 1999 EverQuest
client and supporting scripts, and configures the default startup
command (the same `init` script as the p99bot-os container).

### Project 1999 Client

The Project 1999 client must be acquired separately and located
in a "p99.tar.xz" file in the "tmp" folder of the repository
root (unless one changes `Dockerfile`). This archive file should
contain a "p99" folder which itself contains "eqgame.exe" (and
the other game files). The files should not exist at the root of
the archive.

The typical Titanium client includes many files that aren't needed
on Project 1999. One can find a script posted on the Project 1999
forums called "eqlite" that strips out many of these files:

https://www.project1999.com/forums/showthread.php?t=7186

This script actually leaves some files in place, however, that
can be deleted, and does not delete some files that are created
as a side-effect of running the client (e.g. logs). A copy of the
original script and an updated script that removes more files can
be found in this repository in the `src/tools` directory.

# Operation

## Preparation

For basic information about how to prepare a host to run these
containers, see the main project repo[^1].

Next we will create a script to start the container at boot time:

    sudo touch /root/start-p99 \
      && sudo chmod 700 /root/start-p99 \
      && sudo tee /root/start-p99 <<EOF
    #!/usr/bin/env bash

    while [[ 1 ]]; do
        #
        # Find any defunct xpra processes and kill their parent.
        # This should effectively stop and restart the container.
        #
        p=`ps -eo s,ppid,cmd | grep -E '^Z.+xpra' | awk '{print $2}'`
        if [ -n "${p}" -a "0${p}" -gt 100 ]; then
            kill -9 ${p}
            sleep 30
        fi

        sudo docker container ls \
            | grep p99bot >/dev/null \
            || docker run p99bot-p99-amd64

        sleep 5
    done
    EOF

One will likely want to change the `docker run` command line to
include any additional configuration parameters and possibly a
different container image name.

To start the container on boot, run `sudo crontab -e` and add
this line:

    @reboot /root/start-p99

EverQuest can use up a significant amount of RAM, so it may be
necessary to add some swap space to the host machine. For example,
when using Ubuntu, one can create a 2 GB swap file thusly:

	fallocate -l 2G /swap \
	  && chmod 600 /swap \
	  && mkswap /swap \
	  && swapon /swap \
	  && echo '/swap none swap sw 0 0' >> /etc/fstab

EverQuest rarely uses more than 2 GB of RAM so it is unlikely
to need more than that for swap, but if more or less is desired
simple change the "2G" number in the first command above.

## Requirements

The container requires a P99BOT_ROOT environment variable to be
set. During normal startup this is set by the initialization script
prior to running the other `rc.d` scripts.

## Configuration

The container is configured by setting environment variables when
running the container (e.g. via the `-e` switch when using the
`docker run` command). Available environment variables are:

* **P99BOT_P99_LOGIND_DISABLED**: By default, the p99-login-middlemand
  service is started along with the container and the `eqhost.txt`
  file will be replaced with one that connects to localhost instead
  of the default Project 1999 login server. If this environment
  variable is set to any value, the p99-login-middlemand service
  will NOT start and the normal `eqhost.txt` file and login server
  will be used.
* **P99BOT_P99_XPRA_PASSWORD**: If this environment variable is set
  to any value, xpra will be configured to use that variable as
  a password to authenticate with xpra.

## Usage

For basic information about how to use this container, see the
main project repo[^1].

### Initialization

During normal startup, the initialization scripts start xpra, the
p99-login-middlemand service (if needed), and the Project 1999
EverQuest client app.

The container requires a TCP port to be forwarded to port 8080
inside the container, which provides access to xpra.

To run the container on Linux one can run a command like:

    sudo docker run -p 8080:8080 \
      -e P99BOT_P99_XPRA_PASSWORD=test \
      p99bot-p99-amd64

Windows (WSL) doesn't need the p99-login-middlemand service;
sometimes it actually prevented the server list from populating,
so one can run it without the login service like so:

    sudo docker run -p 8080:8080 \
      -e P99BOT_P99_XPRA_PASSWORD=test \
      -e P99BOT_LOGIND_DISABLED=yes \
      p99bot-p99-amd64

# To-Do

* Run xpra as a non-provileged user, not as root.
* Support using a locked down config file, not environment variables.
* Support SSL certs for the xpra web interface.
* Support changing the listen port for xpra.

---

[^1]: https://gitlab.com/je/play/p99bot
